
import { Injectable } from '@angular/core';
import { LoadingController, ToastController, ModalController } from 'ionic-angular';

@Injectable()
export class Loader {
  public loader : any;
  constructor(
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public modalCtrl: ModalController
  ) {}

  presentLoading(msg){
    this.loader = this.loadingCtrl.create({
      content: msg,
      duration: 10000
    });
    this.loader.present();
  }
  closeLoading(){
    this.loader.dismiss();
  }

  presentToast(msg, detik) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: detik * 1000
    });
    toast.present();
  }
  
  listKategoriModal(page, id) {
    let modal = this.modalCtrl.create(page, id);
    modal.present();
  }

  presentModal(page) {
    let modal = this.modalCtrl.create(page);
    modal.present();
  }

}
