import { Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';


@Injectable()
export class Rest {
  private apiKey = 'f99aecef3d12e02dcbb6260bbdd35189c89e6e73';
  public domain = 'apiskripsimega.desamalangan.com';
  // public domain = 'localhost/admin_resep';
  
  constructor(public http: Http) {}
  
 getDomain(){
   var data = this.domain;
   return data;
 }

  getKue(){
    let headers = new Headers();
    headers.append('X-API-KEY', this.apiKey);
    var url = 'http://'+this.domain+'/index.php/api_kue';
    var response = this.http.get(url, {headers:headers}).map(res => res.json());
    return response;
  }

  getKategoriKue(id, page){
    let headers = new Headers();
    headers.append('X-API-KEY', this.apiKey);
    var url = 'http://'+this.domain+'/index.php/api_kue?id=' + encodeURI(id) + '&page=' + encodeURI(page);
    var response = this.http.get(url, {headers:headers}).map(res => res.json());
    return response;
  }

  search(data){
    let headers = new Headers();
    headers.append('X-API-KEY', this.apiKey);
    var url = 'http://'+this.domain+'/index.php/api_kue?search=' + encodeURI(data);
    var response = this.http.get(url, {headers:headers}).map(res => res.json());
    return response;
  }

  lihat(data){
    let body = data;
    let headers = new Headers();
    headers.append('X-API-KEY', this.apiKey);
    var url = 'http://'+this.domain+'/index.php/api_kue';
    return this.http.post(url, body, {headers:headers});

  }
  
    
}
