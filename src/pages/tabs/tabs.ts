
import { Component } from '@angular/core';

@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = 'HomePage';
  tab2Root = 'KategoriPage';
  tab3Root = 'FavoritPage';
  tab4Root = 'AboutPage';

  constructor() {

  }
}
