import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListKategoriPage } from './list-kategori';

@NgModule({
  declarations: [
    ListKategoriPage,
  ],
  imports: [
    IonicPageModule.forChild(ListKategoriPage),
  ],
})
export class ListKategoriPageModule {}
