import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { Rest } from '../../providers/rest';
import { Loader } from '../../providers/loader';


@IonicPage({
  name: 'List'
})
@Component({
  selector: 'page-list-kategori',
  templateUrl: 'list-kategori.html',
})
export class ListKategoriPage {
  public kategori:any = [];
  public title:any = '';
  domain:any='localhost';
  page = 2;
  id = null;
  maxPage = false;
  reActiveInfinite: any;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public rest: Rest,
    public component: Loader
  ) {
  }

  ionViewWillEnter() {
    this.getDomain();
    this.kategori = this.navParams.get('kategori');
    this.title = this.navParams.get('title');
    this.id = this.navParams.get('id');
  }

  getDomain(){
    this.domain = this.rest.getDomain();
  }

  detailKue(k){
    this.navCtrl.push('DetailKuePage', {data: k})
  }

  close(){
    this.viewCtrl.dismiss();
  }


  loadData(infiniteScroll?){
    if(!infiniteScroll){
      this.component.presentLoading('Sedang Memuat');
    }
      this.rest.getKategoriKue(this.id, this.page).subscribe((data) => {
        this.kategori = this.kategori.concat(data);
        if(!infiniteScroll){
          this.component.closeLoading();
        } else {
          infiniteScroll.complete();
          if(data.length == 0){
              this.maxPage = true;
          }
        } 
      }, (err) => {
        this.component.presentToast(err,5)
        this.component.closeLoading();
      });  
  }


  loadMore(infiniteScroll){
    this.reActiveInfinite = infiniteScroll;
    this.page++;
    this.loadData(infiniteScroll); 

    if (this.maxPage == true){
      infiniteScroll.enable(false);
    }
  }



}
