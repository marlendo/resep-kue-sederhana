import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Rest } from '../../providers/rest';

@IonicPage({
  name: 'SearchPage'
})
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {

  data:any=[];
  domain:any='localhost';
  title:any='Pencarian Resep';
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public rest: Rest
  ) {
  }

  ionViewWillEnter() {
    this.getDomain();
    this.data = this.navParams.get('item');
    this.title = this.navParams.get('title');
  }

  getDomain(){
    this.domain = this.rest.getDomain();
  }

  detailKue(k){
    this.navCtrl.push('DetailKuePage', {data: k})
  }

}
