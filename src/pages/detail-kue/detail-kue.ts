import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Rest } from '../../providers/rest';
import { Loader } from '../../providers/loader';
import { SocialSharing } from '@ionic-native/social-sharing';

@IonicPage({
  name: 'DetailKuePage'
})
@Component({
  selector: 'page-detail-kue',
  templateUrl: 'detail-kue.html',
})
export class DetailKuePage {

  public resep:any={};
  domain:any='localhost';
  statusFav:boolean;
  image = '';
  url = '';  
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public rest: Rest,
    public component: Loader,
    public socialShare: SocialSharing
  ) {
  }

  ionViewWillEnter() {
    this.resep = this.navParams.get('data');
    this.url = 'http://apiskripsimega.desamalangan.com/assets/img/foto_resep/' + this.resep.foto_kue;
    this.image = 'http://apiskripsimega.desamalangan.com/assets/img/foto_resep/' + this.resep.foto_kue;
    this.getDomain();
    this.cekFav();    
    this.innerHTML(this.resep.bahan_kue);
    this.innerHTML(this.resep.langkah_pembuatan);
    this.lihat();        
  }

  getDomain(){
    this.domain = this.rest.getDomain();
  }

  innerHTML(data){
    var map =
      {
          '&amp;': '&',
          '&lt;': '<',
          '&gt;': '>',
          '&quot;': '"',
          '&#039;': "'"
      };
      let v = data.replace(/&amp;|&lt;|&gt;|&quot;|&#039;/g, function(m) {return map[m];});
      data = v;     
  }

  cekFav(){
    if(!localStorage.getItem('fav')){
      this.statusFav = false;
      localStorage.setItem('fav', '[]');
    } else {
      let x = JSON.parse(localStorage.getItem('fav'));
      let y = x.filter(cari => cari.id_kue === this.resep.id_kue);        
      if(y.length == 0){
        this.statusFav = false;
      } else {
        this.statusFav = true;
      }
    }
  }

  simpan(){
    this.statusFav = !this.statusFav;
    console.log(this.statusFav);
    if(this.statusFav == true){
      let x = JSON.parse(localStorage.getItem('fav'));
      x.push(this.resep);
      localStorage.setItem('fav', JSON.stringify(x));
      this.component.presentToast('Horeee Kamu Menambahkan Resep Ini Sebagai Favorit', 3);
    } else {
      let x = JSON.parse(localStorage.getItem('fav'));
      let y = x.filter(cari => cari.id_kue != this.resep.id_kue);        
      localStorage.setItem('fav', JSON.stringify(y));
      this.component.presentToast(this.resep.nama_kue+' Telah Dihapus Dari Daftar Favorit Kamu', 3);
    }
  }

  lihat(){
    let x = {id: this.resep.id_kue, lihat: parseInt(this.resep.dilihat) + 1};
    this.rest.lihat(x).subscribe((data)=>{
      console.log(data);      
    })
  }

  shareWa(){
    this.socialShare.shareViaWhatsApp(this.resep.langkah_pembuatan , this.image, this.url).then(() => {
      console.log('sipp');
    }).catch((err) => {
      console.log(err);
    });
  }

  shareFb(){
    this.socialShare.shareViaFacebook(this.resep.langkah_pembuatan, this.image, this.url).then(() => {
      console.log('sipp');
    }).catch((err) => {
      console.log(err);
    });
  }
  

}
