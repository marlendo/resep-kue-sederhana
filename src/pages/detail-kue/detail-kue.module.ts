import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailKuePage } from './detail-kue';

@NgModule({
  declarations: [
    DetailKuePage,
  ],
  imports: [
    IonicPageModule.forChild(DetailKuePage),
  ],
})
export class DetailKuePageModule {}
