import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';

@IonicPage()
@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
})
export class AboutPage {

  text='Halo bunda yuk download aplikasi resep kue simple ini free looo...';
  link='https://play.google.com/store/apps/details?id=com.resepKue';

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public socialShare: SocialSharing
  ) {
  }

  fb(){
    window.open('https://facebook.com/meygha.suryaningzih1', '_system', 'location=yes');
  }

  wa(){
    window.open('https://api.whatsapp.com/send?phone=6287736282488', '_system', 'location=yes');
  }

  share(){
    this.socialShare.shareViaWhatsApp(this.text , null, this.link).then(() => {
      console.log('sipp');
    }).catch((err) => {
      console.log(err);
    });
  }


}
