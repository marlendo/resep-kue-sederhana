import { TabsPage } from './../tabs/tabs';
import { Loader } from './../../providers/loader';
import { Component } from '@angular/core';
import { IonicPage, NavController, App } from 'ionic-angular';
import { Rest } from './../../providers/rest';

@IonicPage(
  {
    name: 'LoginPage'
  }
)
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  constructor(
    public navCtrl: NavController, 
    public loader: Loader,
    public rest:Rest,
    public app: App
  ) {
  }

  ionViewDidLoad(){
  }

  loginFacebook(){
    console.log('Facebok Login')
  }

  goHome(){
    this.app.getRootNav().setRoot(TabsPage);
  }
  
}
