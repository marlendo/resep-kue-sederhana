import { Loader } from './../../providers/loader';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Rest } from '../../providers/rest';


@IonicPage()
@Component({
  selector: 'page-kategori',
  templateUrl: 'kategori.html',
})
export class KategoriPage {

  public resep:any = [];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public loader: Loader,
    public rest: Rest
  ) {
  }

  ionViewDidLoad() {
    this.resep = JSON.parse(localStorage.getItem('data'));
  }

  kueBasah(){
    this.getKategori(1, 'Kategori Kue Basah');
  }
  kueKering(){
    this.getKategori(2, 'Kategori Kue Kering');
  }

  getKategori(id, title){
    this.loader.presentLoading('Sedang Memuat Kategori');
    this.rest.getKategoriKue(id, 1).subscribe((data)=>{
      this.loader.listKategoriModal('List', {kategori: data, title: title, id: id});
      this.loader.closeLoading();
    })   
  }

}
