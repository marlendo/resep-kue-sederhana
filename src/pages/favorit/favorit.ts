import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Rest } from '../../providers/rest';
import { Loader } from '../../providers/loader';


@IonicPage()
@Component({
  selector: 'page-favorit',
  templateUrl: 'favorit.html',
})
export class FavoritPage {

  resep:any = [];
  domain:any = 'localhost';  
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public rest: Rest,
    public component: Loader
  
  ) {
  }

  ionViewWillEnter() {
    this.getDomain();
    this.getData();
  }

  getDomain(){
   this.domain = this.rest.getDomain();
  }

  getData(){
    this.resep = JSON.parse(localStorage.getItem('fav'));
    if(this.resep.length == 0){
      this.component.presentToast('Sepertinya Kamu Belum Menambahkan Resep di Favorit Kamu', 3)
    }    
   }

   detailKue(k){
     this.navCtrl.push('DetailKuePage', {data: k})
   }


}
