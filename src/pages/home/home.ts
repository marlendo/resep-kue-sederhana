import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Rest } from '../../providers/rest';
import { Loader } from '../../providers/loader';


@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  resep:any = [];
  domain:any = 'localhost';
  searchData:any='';
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public rest: Rest,
    public component: Loader
  
  ) {
  }

  ionViewWillEnter() {
    this.getDomain();
    if(this.resep.length == 0){
      this.getData();
    }
  }

  getDomain(){
   this.domain = this.rest.getDomain();
  }

  getData(){
    this.component.presentLoading('Sedang Memuat Data')
    this.rest.getKue().subscribe((data)=>{
      this.resep = data;
      console.log(data);
      this.component.closeLoading();
    })
   }

   detailKue(k){
     this.navCtrl.push('DetailKuePage', {data: k})
   }

   search(){
     this.component.presentLoading('Pencarian Data');
     this.rest.search(this.searchData).subscribe((hasil)=>{
      this.navCtrl.push('SearchPage', {item: hasil, title: this.searchData});
      this.component.closeLoading();
     })
   }
 

}
